#ifndef PROGRAMS_DELAYUTILS_H_
#define PROGRAMS_DELAYUTILS_H_

#include <debug.h>
/**
 * return 1 if elapsed period time since instant else 0
 */
uint8 delayUtils_elapsed(uint32 instant, uint32 period);

#endif