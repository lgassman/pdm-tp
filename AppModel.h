#ifndef PROGRAMS_APP_MODEL_H_
#define PROGRAMS_APP_MODEL_H_
#include <debug.h>
#include "DataModel.h"

struct AppModel;
typedef struct AppModel AppModel;

typedef void (*AppEventHandler)(AppModel *, void *);

#define MODE_SIZE 3

typedef enum AppModel_Mode {
    ATT1, ATT2, TRANSMISSION
} AppModel_Mode;


typedef struct AppEventHandlers {
    AppEventHandler onChangeMode;
    AppEventHandler onAction;
    AppEventHandler onTick;
    AppEventHandler onReset;    
    void * data;
} AppEventHandlers;


struct AppModel {
	AttDataModel data;
	AppModel_Mode mode;
    AppEventHandlers handlers[MODE_SIZE];
    uint16 colors[MODE_SIZE];
};

void appModel_initModule(void);
void appModel_configMode(AppModel * model, AppModel_Mode mode, uint16 color, AppEventHandlers handlers);
void appModel_init(AppModel * model);

void appModel_changeMode(AppModel * model);
void appModel_action(AppModel * model);
void appModel_reset(AppModel * model);
void appModel_tick(AppModel * model);

uint16 appModel_currentColor(AppModel * model);
uint16 appModel_currentAttribute(AppModel * model);

#endif 
