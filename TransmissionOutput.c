#include "TransmissionOutput.h"
#include "Colors.h"
#include <time.h>
#include "DelayUtils.h"


void transmissionOutput_nullEventHandler(AppModel * model, void * nullPointer);
void transmissionOutput_turnOn(AppModel * model, void * nullPointer);
void transmissionOutput_onTick(AppModel * model, void * transmissionOutputData);

void transmissionOutput_init(AppEventHandlers * handlers, TransmissionOutputData * data){
    handlers->onChangeMode = transmissionOutput_turnOn;
    handlers->onAction = transmissionOutput_turnOn;
    handlers->onTick = transmissionOutput_onTick;
    handlers->onReset = transmissionOutput_nullEventHandler;
    handlers->data=(void *)data;
    data->lastEventTime = TimeGet32();
    data->eventDelay = 100 * MILLISECOND ;
    data->nextColor = BLACK ;    
}

void transmissionOutput_nullEventHandler(AppModel * model, void * transmissionOutputData) {
}


void transmissionOutput_turnOn(AppModel * model, void * transmissionOutputData) {
    uint16 color = appModel_currentColor(model);
    colors_turnOn(color);
    TransmissionOutputData * data = (TransmissionOutputData *)transmissionOutputData;
    data->lastEventTime = TimeGet32();
    data->nextColor = BLACK ;    
}

void transmissionOutput_onTick(AppModel * model, void * transmissionOutputData) {
    if(model->data.sending) {
         TransmissionOutputData * data = (TransmissionOutputData *)transmissionOutputData;
         if(delayUtils_elapsed(data->lastEventTime, data->eventDelay)){
            colors_turnOn(data->nextColor);
            data->nextColor = (data->nextColor == BLACK) ? appModel_currentColor(model) : BLACK;
            data->lastEventTime = TimeGet32();
         }
    }
}
