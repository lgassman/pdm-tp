#ifndef PROGRAMS_TRANSMISSION_H_
#define PROGRAMS_TRANSMISSION_H_

#include <debug.h>

void transmission_startAdvertising(uint16 att1, uint16 att2);
void transmission_stopAdvertising(void);

#endif