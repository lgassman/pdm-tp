/******************************************************************************
 *  Copyright (c) 2012 - 2017 Qualcomm Technologies International, Ltd.
 *  Part of CSR uEnergy SDK 2.6.3
 *  Application version 2.6.3.0
 *
 *  FILE
 *      main.c
 *
 *  DESCRIPTION
 *      Simple PIO example to show PIO usage. No explicit debouncing is applied
 *      while processing the button events.
 *
 ******************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
 
#include <main.h>           /* Functions relating to powering up the device */
#include <ls_app_if.h>      /* Link Supervisor application interface */
#include <debug.h>          /* Simple host interface to the UART driver */
#include <pio.h>            /* Programmable I/O configuration and control */
#include "PioConstants.h"
#include "Button.h"
#include "AppModel.h"
#include "TransmissionOutput.h"
#include "Colors.h"
#include "AttributeOutput.h"
#include <timer.h>          /* Chip timer functions */
#include "Transmission.h"

/*============================================================================*
 *  Private Definitions
 *============================================================================*/

void changeMode(void * appModel);
void action(void * appModel);
void reset(void * appModel);
void timerCallback(timer_id const id);


/* Number of timers used in this application */
#define MAX_TIMERS 1
#define TIMER_TIMEOUT (50 * MILLISECOND) //1 tick cada 50 ms
        
/* Declare timer buffer to be managed by firmware library */
static uint16 app_timers[SIZEOF_APP_TIMER * MAX_TIMERS];

static Button buttonMode;
static Button buttonAction;
static AppModel model;

static AppEventHandlers attEventHandlers;
static AppEventHandlers transmissionEventHandlers;

static AttributeOutputData attHandlerData;
static TransmissionOutputData transmissionOutputData;



/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppPowerOnReset
 *
 *  DESCRIPTION
 *      This user application function is called just after a power-on reset
 *      (including after a firmware panic), or after a wakeup from Hibernate or
 *      Dormant sleep states.
 *
 *      At the time this function is called, the last sleep state is not yet
 *      known.
 *
 *      NOTE: this function should only contain code to be executed after a
 *      power-on reset or panic. Code that should also be executed after an
 *      HCI_RESET should instead be placed in the AppInit() function.
 *
 * PARAMETERS
 *      None
 *
 * RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
void AppPowerOnReset(void)
{
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppInit
 *
 *  DESCRIPTION
 *      This user application function is called after a power-on reset
 *      (including after a firmware panic), after a wakeup from Hibernate or
 *      Dormant sleep states, or after an HCI Reset has been requested.
 *
 *      NOTE: In the case of a power-on reset, this function is called
 *      after app_power_on_reset().
 *
 * PARAMETERS
 *      last_sleep_state [in]   Last sleep state
 *
 * RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/


void changeMode(void * appModel) {
    appModel_changeMode((AppModel *)appModel);
}

void action(void * appModel) {
    appModel_action((AppModel *)appModel);
}

void reset(void * appModel) {
    appModel_reset((AppModel *)appModel);
}

void startTimer(uint32 timeout, timer_callback_arg handler);

void timerCallback(timer_id const id) {
    appModel_tick(&model);    
    TimerCreate(TIMER_TIMEOUT, TRUE, timerCallback);
}



void AppInit(sleep_state last_sleep_state)
{
    appModel_initModule();    
    
    uint16 rgbPins = (1UL << PIO_R) | (1UL << PIO_G) | (1UL << PIO_B);
    uint16 buttonsPins = (1UL << PIO_ACTION_BUTTON) | (1UL << PIO_SELECT_MODE_BUTTON);
        
    /* Set RGBto be controlled directly via PioSet */
    PioSetModes(rgbPins, pio_mode_user);

    /* Configure RGB to be outputs */
    PioSetDir(PIO_R, PIO_DIR_OUTPUT);
    PioSetDir(PIO_G, PIO_DIR_OUTPUT);
    PioSetDir(PIO_B, PIO_DIR_OUTPUT);

    /* Set the LED0 and LED1 to have strong internal pull ups */
    PioSetPullModes(rgbPins, pio_mode_strong_pull_up);

    /* Configure buttons  to be controlled directly */
    PioSetModes(buttonsPins, pio_mode_user); 
    
    
    /* Configure button to be input */
    PioSetDir(PIO_ACTION_BUTTON, PIO_DIR_INPUT);
    PioSetDir(PIO_SELECT_MODE_BUTTON, PIO_DIR_INPUT);
    
    /* Set weak pull up on button PIO, in order not to draw too much current
     * while button is pressed
     */
    PioSetPullModes(buttonsPins, pio_mode_weak_pull_up);

    /* Set the button to generate sys_event_pio_changed when pressed as well
     * as released
     */
    PioSetEventMask(buttonsPins, pio_event_mode_both);
      
    appModel_init(&model);    
      
    
    button_init(&buttonMode, PIO_SELECT_MODE_BUTTON, &model);
    button_init(&buttonAction, PIO_ACTION_BUTTON, &model);

    
    attributeOutput_init(&attEventHandlers, &attHandlerData);    
    transmissionOutput_init(&transmissionEventHandlers, &transmissionOutputData);
    
    appModel_configMode(&model, ATT1, GREEN, attEventHandlers);
    appModel_configMode(&model, ATT2, WHITE, attEventHandlers);
    appModel_configMode(&model, TRANSMISSION, BLUE, transmissionEventHandlers);
    
    
    button_onPress(&buttonMode, changeMode);
    button_onPress(&buttonAction, action);
    button_onHold(&buttonAction, reset, 3);
    
   

   appModel_changeMode(&model);
   
   TimerInit(MAX_TIMERS, (void *)app_timers);
   TimerCreate(TIMER_TIMEOUT, TRUE, timerCallback);
   
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcesSystemEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a system event, such
 *      as a battery low notification, is received by the system.
 *
 * PARAMETERS
 *      id   [in]   System event ID
 *      data [in]   Event data
 *
 * RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
void AppProcessSystemEvent(sys_event_id id, void *data)
{
    
    button_handle_event(&buttonMode, id, data);
    button_handle_event(&buttonAction, id, data);
   
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcessLmEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a LM-specific event
 *      is received by the system.
 *
 * PARAMETERS
 *      event_code [in]   LM event ID
 *      event_data [in]   LM event data
 *
 * RETURNS
 *      TRUE if the app has finished with the event data; the control layer
 *      will free the buffer.
 *----------------------------------------------------------------------------*/
bool AppProcessLmEvent(lm_event_code event_code, LM_EVENT_T *event_data)
{
    return TRUE;
}
