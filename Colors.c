#include "Colors.h"
#include <pio.h>

void colors_turnOn(uint16 color) {
    PioSet(PIO_R, (color & ( 1UL << PIO_R)) == 0);
    PioSet(PIO_G, (color & ( 1UL << PIO_G)) == 0);
    PioSet(PIO_B, (color & ( 1UL << PIO_B)) == 0);
}