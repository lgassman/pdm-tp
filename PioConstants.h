#ifndef PROGRAMS_PIO_CONSTANTS_H_
#define PROGRAMS_PIO_CONSTANTS_H_

#define PIO_SELECT_MODE_BUTTON      0          /* PIO connected to the button on CSR10xx */
#define PIO_ACTION_BUTTON      1          /* PIO connected to the button on CSR10xx */

#define PIO_R        9          /* PIO connected to the LEDR on CSR10xx */
#define PIO_G        10           /* PIO connected to the LEDG on CSR10xx */
#define PIO_B        11           /* PIO connected to the LEDG on CSR10xx */



#define PIO_DIR_OUTPUT  TRUE        /* PIO direction configured as output */
#define PIO_DIR_INPUT   FALSE       /* PIO direction configured as input */


#endif 
