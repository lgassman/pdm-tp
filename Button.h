/**
 * Button.h
 *
 *  Created on: Jun 1, 2019
 *      Author: leo
 *
 * Es un TAD que se crea a partir de un un modelo y un pin gpio.
 * Adem'qs se le configura las funciones onPress y OnRelease que son invocadas
 * cuando corresponden pasando el modelo como par�metro en un puntero a void.
 * Puede no configurarse alguna funci�n si no se desea
 */

#ifndef PROGRAMS_BUTTON_H_
#define PROGRAMS_BUTTON_H_

#include <debug.h>
#include <sys_events.h>


typedef void (*ButtonEventHandler)(void *);

typedef struct Button {
	void * model;
	uint16 gpioPin;
	ButtonEventHandler onPress;
	ButtonEventHandler onHold;
    uint16 secondsToHold;
    uint32 lastEvent;
} Button;



void button_init(Button * button, uint16 gpioPin, void * model);

void button_onPress(Button * button, ButtonEventHandler handler);
void button_onHold(Button * button, ButtonEventHandler eventHandler, uint16 secondsToHold);
void button_handle_event(Button * button, sys_event_id id, void *data);

#endif /* PROGRAMS_SEMAFORO_INC_BUTTON_H_ */
