
#include "AppModel.h"
#include "Transmission.h"

static AppModel_Mode nextMode[3];

void appModel_initModule() {
    nextMode[ATT1] = ATT2;
    nextMode[ATT2] = TRANSMISSION; 
    nextMode[TRANSMISSION] = ATT1;
}

void appModel_configMode(AppModel * model, AppModel_Mode mode, uint16 color, AppEventHandlers handlers) {
    model->handlers[mode] = handlers;
    model->colors[mode] = color;                        
}

void appModel_init(AppModel * model) {
    model->data.attribute1 = 0;
    model->data.attribute2 = 0;
    model->data.sending = 0;    
    model->mode = TRANSMISSION;
}

void appModel_changeMode(AppModel * model) {
    model->mode = nextMode[model->mode];
    
    AppEventHandlers handlers = model->handlers[model->mode];
    handlers.onChangeMode(model, handlers.data);    
}

void appModel_action(AppModel * model) { 
    switch(model->mode) {
        case ATT1: {
            model->data.attribute1++;
            break;
        }
        case ATT2: {
            model->data.attribute2++;
            break;
        }
        case TRANSMISSION: {
            if(model->data.sending) {
                transmission_stopAdvertising();
            }
            else {
                transmission_startAdvertising(model->data.attribute1, model->data.attribute2);            
            }
            model->data.sending = !model->data.sending;
            break;
        }
            
    }
    
    AppEventHandlers handlers = model->handlers[model->mode];
    handlers.onAction(model, handlers.data);    
}

void appModel_reset(AppModel * model) {
    switch(model->mode) {
        case ATT1: {
            model->data.attribute1 = 0;
            break;
        }
        case ATT2: {
            model->data.attribute2 = 0;
            break;
        }
        case TRANSMISSION: {
            //nothing
            break;
        }
    }    
    AppEventHandlers handlers = model->handlers[model->mode];
    handlers.onReset(model, handlers.data);    
}


void appModel_tick(AppModel * model) {
    AppEventHandlers handlers = model->handlers[model->mode];
    handlers.onTick(model, handlers.data);    
}

uint16 appModel_currentColor(AppModel * model) {
    return model->colors[model->mode];
}

uint16 appModel_currentAttribute(AppModel * model) {
    switch(model->mode) {
        case ATT1: {
            return model->data.attribute1;
        }
        case ATT2: {
           return model->data.attribute2;
        }
        default : {
            return 0;
        };
    }      
}
