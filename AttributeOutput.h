#ifndef PROGRAMS_ATTRIBUTE_OUTPUT_H_
#define PROGRAMS_ATTRIBUTE_OUTPUT_H_

#include <debug.h>
#include "AppModel.h"
#include <pio.h>

typedef enum AttributeOutputState {
    ESPERANDO,
    PARPADEANDO,
} AttributeOutputState;

typedef struct AttributeOutputData{
    uint32 lastEventTime;
    uint32 eventDelay;    
    AttributeOutputState currentState; 
    uint16 maxIteraciones;    
    uint16 currentIt ;    
    uint8 encendido;
} AttributeOutputData;

void attributeOutput_init(AppEventHandlers * handlers, AttributeOutputData * eventData);

#endif 
