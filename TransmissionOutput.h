#ifndef PROGRAMS_CONSTANT_COLOR_EVENT_HANDLERS_H_
#define PROGRAMS_CONSTANT_COLOR_EVENT_HANDLERS_H_

#include <debug.h>
#include "AppModel.h"

typedef struct TransmissionOutputData{
    uint32 lastEventTime;
    uint32 eventDelay;    
    uint16 nextColor;
} TransmissionOutputData;

void transmissionOutput_init(AppEventHandlers * handlers, TransmissionOutputData * data);

#endif 
