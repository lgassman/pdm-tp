#include "transmission.h"
#include <gap_app_if.h>
#include <gap_types.h>
//static char packet[23];

static uint8 serviceAdStructure[10];
static uint8 shortNameAdStructure[4];
static uint8 longNameAdStructure[8];

void transmission_startAdvertising(uint16 att1, uint16 att2) {
    //ad type    
    serviceAdStructure[0] = AD_TYPE_SERVICE_SOLICIT_UUID_128BIT; 
    //service id
    serviceAdStructure[1] = 0x1E; 
    serviceAdStructure[2] = 0x01; 
    serviceAdStructure[3] = 0xE0; 
    serviceAdStructure[4] = 0x1E; 
    serviceAdStructure[5] = 0x01; 

    //atributes
    serviceAdStructure[6] = att1 >> 8;
    serviceAdStructure[7] = 0x00FF & att1;
    serviceAdStructure[8] = att2 >> 8;
    serviceAdStructure[9] = 0x00FF & att2;
    
    shortNameAdStructure[0] = AD_TYPE_LOCAL_NAME_SHORT;
    shortNameAdStructure[1] = 'L';
    shortNameAdStructure[2] = 'E';
    shortNameAdStructure[3] = 'O';
    
    longNameAdStructure[0] = AD_TYPE_LOCAL_NAME_COMPLETE;
    longNameAdStructure[1] = 'L';
    longNameAdStructure[2] = 'E';
    longNameAdStructure[3] = '0';
    longNameAdStructure[4] = '-';
    longNameAdStructure[5] = 'P';
    longNameAdStructure[6] = 'D';
    longNameAdStructure[7] = 'M';

    
     GapSetMode(gap_role_broadcaster,
               gap_mode_discover_no,
               gap_mode_connect_no,
               gap_mode_bond_no,
               gap_mode_security_none);    
    
    
    TYPED_BD_ADDR_T address;
    address.type = ls_addr_type_public;
    address.addr.nap = 0x0002;
    address.addr.uap = 0x5b;
    address.addr.lap = 0x0419a2;

    GapSetAdvAddress(&address);
    
    /* set the GAP Broadcaster role */
    GapSetMode(gap_role_broadcaster,
               gap_mode_discover_no,
               gap_mode_connect_no,
               gap_mode_bond_no,
               gap_mode_security_none);
    
    
    /* clear the existing advertisement data, if any */
    LsStoreAdvScanData(0, NULL, ad_src_advertise);

    /* set the advertisement interval, API accepts the value in microseconds */
    GapSetAdvInterval(20 * MILLISECOND, 500 * MILLISECOND);
  

    /* store the advertisement data */
    LsStoreAdvScanData(10, serviceAdStructure, ad_src_advertise);
    LsStoreAdvScanData(4, shortNameAdStructure, ad_src_advertise);
    LsStoreAdvScanData(8, longNameAdStructure, ad_src_advertise);

    
    /* Start broadcasting */
    LsStartStopAdvertise(TRUE, whitelist_disabled, ls_addr_type_public);
}
    
void transmission_stopAdvertising(void) {
    LsStartStopAdvertise(FALSE, whitelist_disabled, ls_addr_type_public);
}
