#ifndef PROGRAMS_COLORS_H_
#define PROGRAMS_COLORS_H_

#include "PioConstants.h"
#include <debug.h>

#define BLACK 0
#define GREEN 1UL << PIO_G
#define BLUE 1UL << PIO_B
#define WHITE (1UL << PIO_R) | (1UL << PIO_G) | (1UL << PIO_B)


void colors_turnOn(uint16 color);

#endif 
