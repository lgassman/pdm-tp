/*
 * Button.c
 *
 *  Created on: Jun 1, 2019
 *      Author: leo
 */

#include "Button.h"
#include <pio.h>
#include <time.h>


void nullHandler(void *);

void button_init(Button * button, uint16 gpioPin, void * model) {

	button->model = model;
	button->gpioPin = gpioPin;
	button->onPress = nullHandler;
	button->onHold = nullHandler;
	button->lastEvent = TimeGet32();
	button->secondsToHold = 0xFFFF;

}


void nullHandler(void * model){
}


void button_onPress(Button * button, ButtonEventHandler eventHandler) {
	button->onPress = eventHandler;
}

void button_onHold(Button * button, ButtonEventHandler eventHandler, uint16 secondsToHold) {
	button->onHold = eventHandler;
    button->secondsToHold = secondsToHold;
}

void button_handle_event(Button * button, sys_event_id id, void *data) {
    if (id == sys_event_pio_changed) {
        const pio_changed_data *pPioData = (const pio_changed_data *)data;
        
        /* If the PIO event comes from the button */
        if (pPioData->pio_cause & (1UL << button->gpioPin)){
            
            /* If PIO was HIGH when this event was generated (that means
             * button was released, generating a rising edge causing PIO
             * to go from LOW to HIGH)
             */
            if (pPioData->pio_state & (1UL << button->gpioPin))
            {
                
                uint32 now = TimeGet32();
                //si now es menor es que se acabo el tiempo y hubo overflow. 
                //es muy poco probable que un caso asi ocurre, y si ocurre lo m�s probable es que 
                //sea un hold (el clock de 32 bits es de microsegundos, maneja aproximadamente 9 minutos y algo mas)
                //por eso lo manejo como un hold. No vale la pena para este TP contemplar estos casos raros
                if( now < button->lastEvent || ((now - button->lastEvent) > button->secondsToHold * SECOND)) {
                    button->onHold(button->model);
                }
                else {
                    button->onPress(button->model);
                }
            }
            else
            /* If PIO was LOW when this event was generated (that means
             * button was pressed, generating a falling edge causing
             * PIO to go from HIGH to LOW)
             */
            {   
                button->lastEvent =  TimeGet32();
            }
        }
    }
}




