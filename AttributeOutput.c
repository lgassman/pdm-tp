#include "AttributeOutput.h"
#include "Colors.h"
#include <time.h>
#include "DelayUtils.h"

#define INCREMENTANDO_DELAY 200 * MILLISECOND;
#define RESETEANDO_DELAY 500 * MILLISECOND;
#define RESETEANDO_ITERACIONES 3;
        
void attributeOutput_initEMF(AppModel * model, void * attributeOutputData);
void attributeOutput_onAction(AppModel * model, void * attributeOutputData);
void attributeOutput_onReset(AppModel * model, void * attributeOutputData);
void attributeOutput_onTick(AppModel * model, void * attributeOutputData);


void attributeOutput_init(AppEventHandlers * handlers, AttributeOutputData * eventData) {
    handlers->onChangeMode = attributeOutput_initEMF;
    handlers->onAction = attributeOutput_onAction;
    handlers->onTick = attributeOutput_onTick;
    handlers->onReset = attributeOutput_onReset;
    handlers->data = eventData;
    eventData->currentState = ESPERANDO;
}


void attributeOutput_initEMF(AppModel * model, void * attributeOutputData) {
    AttributeOutputData * data = (AttributeOutputData *)attributeOutputData;

    data->currentState = ESPERANDO;
    
    uint16 color = appModel_currentColor(model);
    colors_turnOn(color);
}

void attributeOutput_onAction(AppModel * model, void * attributeOutputData) {

    AttributeOutputData * data = (AttributeOutputData *)attributeOutputData;

    data->currentState = PARPADEANDO;
    data->lastEventTime = TimeGet32();
    data->maxIteraciones = appModel_currentAttribute(model);
    data->currentIt = 0;
    data->encendido = 0;
    data->eventDelay = INCREMENTANDO_DELAY;
    colors_turnOn(BLACK);
}

void attributeOutput_onReset(AppModel * model, void * attributeOutputData) {

    AttributeOutputData * data = (AttributeOutputData *)attributeOutputData;

    data->currentState = PARPADEANDO;
    data->lastEventTime = TimeGet32();
    data->maxIteraciones = RESETEANDO_ITERACIONES;
    data->currentIt = 0;
    data->encendido = 0;
    data->eventDelay = RESETEANDO_DELAY;
    colors_turnOn(BLACK);
}

void attributeOutput_onTick(AppModel * model, void * attributeOutputData) {
    AttributeOutputData * data = (AttributeOutputData *)attributeOutputData;
    if(data->currentState == ESPERANDO) {
        return;
    }
    
    if(delayUtils_elapsed(data->lastEventTime, data->eventDelay)) {
        
        if(data->encendido) {  
           data->currentIt++;
           colors_turnOn(BLACK);
           
        }
        else {
            //encender
            colors_turnOn(appModel_currentColor(model));
            if(data->currentIt >= data->maxIteraciones - 1 ) {
                data->currentState = ESPERANDO;
            }
        }
        data->encendido = !data->encendido;
        data->lastEventTime = TimeGet32();
   }

}