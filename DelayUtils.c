#include "DelayUtils.h"
#include <time.h> 

uint8 delayUtils_elapsed(uint32 instant, uint32 period) {
    
    uint32 now = TimeGet32();
    uint32 delay;
    if(now < instant ) {
         //overflow del timer
        delay = (0xFFFF - instant) + now;
    }
    else {
        delay =  now - instant;
    }
    
    return delay >= period;
}
