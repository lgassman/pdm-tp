/**
 * Button.h
 *
 *  Created on: Jun 1, 2019
 *      Author: leo
 *
 * Es un TAD que se crea a partir de un un modelo y un pin gpio.
 * Adem'qs se le configura las funciones onPress y OnRelease que son invocadas
 * cuando corresponden pasando el modelo como par�metro en un puntero a void.
 * Puede no configurarse alguna funci�n si no se desea
 */

#ifndef PROGRAMS_DATA_MODEL_H_
#define PROGRAMS_DATA_MODEL_H_

#include <debug.h>

typedef struct AttDataModel {
	int16 attribute1;
	int16 attribute2;
    int8 sending;
} AttDataModel;

#endif 
